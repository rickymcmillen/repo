# `$ repo`

Open directories/files/commits in repositories hosting provider.

It inspects the current directory for the Remote Git Origin (compatible with GitHub & BitBucket) and constructs the relevant URL to open in the default browser.

## Usage
```
Options
    -f, --file PATH                  Path to directory/file
    -b, --branch BRANCH              Branch to use when opening directory/file. Defaults to 'master'
    -c, --commit HASH                Commit to show
        --output [TYPE]              Output format (browser, list). Defaults to 'browser'
    -v, --version                    Display version number
    -h, --help                       Prints this help
```

## Examples
```
$ repo -f lib/repo/configuration.rb
# Opens the following URL in the browser, using the default branch
https://bitbucket.org/rickymcmillen/repo/src/master/lib/repo/configuration.rb

$ repo -b dev -f lib/repo/configuration.rb
# Opens the following URL in the browser, using the 'dev' branch
https://bitbucket.org/rickymcmillen/repo/src/dev/lib/repo/configuration.rb

$ repo -c 9a1e7de91b94c --output list
=> https://bitbucket.org/rickymcmillen/repo/commits/9a1e7de91b94c
# Prints URL to STDOUT
```

## Configuration
You can configure what `repo` will use as the default branch.

Add a configuration file in your home directory called `.repo.rc.yaml`. Eg. To change the default branch to 'staging'

```
$ echo "---\nbranch: staging" > ~/.repo.rc.yaml
```

## Development & Installation
  - Clone repository
  - `cd` to project directory
  - `rake install:local`
