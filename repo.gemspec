# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'repo/version'

Gem::Specification.new do |spec|
  spec.name          = "repo"
  spec.version       = Repo::VERSION
  spec.authors       = ["Ricky McMillen"]
  spec.email         = ["ricky@rickymcmillen.co.uk"]

  spec.summary       = %q{Open directories/files/commits in the repositories hosting provider.}
  spec.homepage      = "http://bitbucket.org/rickymcmillen/repo"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.executables   = ["repo"]
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
end
