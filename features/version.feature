Feature: We can view the version number.
  As a busy developer
  I want to be able to know what version of the app I am using
  so that I can do my job more reliably.

Scenario: Display Version
  When I successfully run `repo --version`
  Then the stdout should contain "0.0.1"
