Feature: We can open files on the repository host.
  As a busy developer
  I want to be able to open files on my repository host without leaving a CLI
  so that I spend more time in my favourite place.

Scenario: Opening files
  When I successfully run `repo`
  Then the stdout should contain "https://bitbucket.org/rickymcmillen/repo/src/master/lib/repo/browser.rb"
