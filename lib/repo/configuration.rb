require 'yaml'

module Repo
  class Configuration

    SENSIBLE_DEFAULTS = {branch: "master"}.freeze
    CONFIG_FILE = File.join(ENV['HOME'], ".repo.rc.yaml")

    def self.options
      new.options
    end

    def options
      robust_merge(SENSIBLE_DEFAULTS, configuration_options)
    end

    private
    def configuration_options
      File.exists?(CONFIG_FILE) ? YAML.load_file(CONFIG_FILE) : {}
    end

    def robust_merge(hash_a, hash_b)
      symbolise_keys(hash_a).merge!(symbolise_keys(hash_b))
    end

    def symbolise_keys(hash)
      hash.each_with_object({}){|(k,v), h| h[k.to_sym] = v}
    end

  end
end
