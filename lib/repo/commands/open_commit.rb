require 'uri'
require_relative './../remote_repository'

module Repo
  class OpenCommit

    def initialize(commit)
      @url = URI(RemoteRepository.indentify.open_commit_url(commit))
    end

    def url
      @url
    end

  end
end
