require 'uri'
require_relative './../remote_repository'

module Repo
  class OpenFile

    def initialize(branch, path)
      @url = URI(RemoteRepository.indentify.open_file_url(branch, path))
    end

    def url
      @url
    end

  end
end
