module Repo
  class Error

    def self.puts(message)
      STDERR.puts("error: #{message}")
    end

  end
end
