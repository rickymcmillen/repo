module Repo
  module Repositories
    class Github

      OPEN_FILE_URL_FORMAT = "blob/{BRANCH}/{PATH}".freeze
      OPEN_COMMIT_URL_FORMAT = "commit/{COMMIT}".freeze

      def initialize(profile, repo_name)
        @base = "https://github.com/#{profile}/#{repo_name}/"
      end

      def open_file_url(branch, path)
        @base + OPEN_FILE_URL_FORMAT.gsub("{BRANCH}", branch).gsub("{PATH}", path)
      end

      def open_commit_url(commit)
        @base + OPEN_COMMIT_URL_FORMAT.gsub("{COMMIT}", commit)
      end

    end
  end
end
