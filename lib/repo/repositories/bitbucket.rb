module Repo
  module Repositories
    class Bitbucket

      OPEN_FILE_URL_FORMAT = "src/{BRANCH}/{PATH}".freeze
      OPEN_COMMIT_URL_FORMAT = "commits/{COMMIT}".freeze

      def initialize(profile, repo_name)
        @base = "https://bitbucket.org/#{profile}/#{repo_name}/"
      end

      def open_file_url(branch, path)
        @base + OPEN_FILE_URL_FORMAT.gsub("{BRANCH}", branch).gsub("{PATH}", path)
      end

      def open_commit_url(commit)
        @base + OPEN_COMMIT_URL_FORMAT.gsub("{COMMIT}", commit)
      end

    end
  end
end
