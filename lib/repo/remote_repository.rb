require 'uri'
require_relative './error'
require_relative './repositories/github'
require_relative './repositories/bitbucket'

module Repo
  class RemoteRepository

    attr_reader :host

    def initialize
      if git_remote.empty?
        Error.puts('There is no Git remote origin')
        exit(2)
      end

      @host = indentify
    end

    def self.indentify
      new.host
    end

    private
    def indentify
      case git_remote
      when /github/
        Repositories::Github.new(profile_name, repository_name)
      when /bitbucket/
        Repositories::Bitbucket.new(profile_name, repository_name)
      else
        Error.puts('Unkown git repository')
        exit(3)
      end
    end

    def profile_name
      repository_location.split("/").first
    end

    def repository_name
      repository_location.split("/").last
    end

    def repository_location
      git_remote.split(":").last.chomp(".git")
    end

    def git_remote
      @git_remote ||= %x[git config --get remote.origin.url].chomp
    end

  end
end
