require 'optparse'
require_relative './version'
require_relative './configuration'
require_relative './formatter/browser'
require_relative './commands/open_file'
require_relative './commands/open_commit'
require_relative './error'

module Repo
  class Repo

    PROGRAM_NAME = File.basename($PROGRAM_NAME).freeze
    OUTPUT = {browser: Browser, list: STDOUT }.freeze

    attr_reader :options, :opt_parser

    def initialize
      @options = Configuration.options
    end

    def parse!
      @opt_parser = OptionParser.new do |opt|
        opt.banner = "Usage: #{PROGRAM_NAME} [OPTIONS]"
        opt.separator ""
        opt.separator "  Open directories/files/commits in the repositories hosting provider."
        opt.separator ""
        opt.separator "Options"

        opt.on("-f", "--file PATH", "Path to directory/file") do |path|
          options[:path] = path
        end

        opt.on("-b", "--branch BRANCH", "Branch to use when opening directory/file. Defaults to 'master'") do |branch|
          options[:branch] = branch
        end

        opt.on("-c", "--commit HASH", "Commit to show") do |commit|
          options[:commit] = commit
        end

        opt.on("--output [TYPE]", [:browser, :list], "Output format (browser, list). Defaults to 'browser'") do |type|
          options[:output] = OUTPUT[type]
        end

        opt.on("-v", "--version", "Display version number") do
          puts VERSION
          exit
        end

        opt.on("-h", "--help", "Prints this help") do
          puts opt
          exit
        end
      end

      opt_parser.parse!

      self
    end

    def execute
      unless Dir.exists?(".git")
        Error.puts("This is not a Git repository")
        exit(1)
      end

      if options[:commit]
        handle_output OpenCommit.new(options[:commit]).url
      elsif options[:path]
        handle_output OpenFile.new(options[:branch], options[:path]).url
      else
        puts opt_parser.help
      end
    end

    private
    def handle_output(url)
      output = options.fetch(:output){ STDOUT.tty? ? OUTPUT[:browser] : OUTPUT[:list] }
      output.puts(url)
    end
  end
end
